<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

  <xsl:param name="latex.class.book">myclass</xsl:param>

  <!-- Layout for printing on US Letter paper -->
  <!-- -->
  <!-- Should be equivalent to next (commented) line, and should match the -->
  <!-- default behaviour, but it didn't work...  -->
  <!-- -->
  <!-- <xsl:param name="paper.type">USletter</xsl:param> -->
  <xsl:param name="page.width">8.5in</xsl:param>
  <xsl:param name="page.height">11in</xsl:param>

  <xsl:param name="page.margin.inner">1.5in</xsl:param>
  <xsl:param name="page.margin.outer">1.5in</xsl:param>

  <xsl:param name="page.margin.top">1in</xsl:param>
  <xsl:param name="page.margin.bottom">1in</xsl:param>

  <xsl:param name="double.sided">1</xsl:param>
  <xsl:param name="latex.class.options">openright,twoside</xsl:param>

  <xsl:param name="latex.encoding">utf8</xsl:param>

  <xsl:param name="preface.tocdepth">1</xsl:param>

  <xsl:param name="toc.section.depth">0</xsl:param>

  <!-- do not list figures -->
  <xsl:param name="doc.lot.show">example</xsl:param>
<!--
Make final page blank, which is required for PDFs inteneded for
extended distribution with LuLu.
-->

  <xsl:param name="latex.enddocument">
    <xsl:text>\pagebreak
\thispagestyle{empty}
~
\end{document}
    </xsl:text>
  </xsl:param>

  <!-- Attributions should be italicized, right-aligned -->
  <xsl:template match="attribution">
    <xsl:param name="content">
      <xsl:apply-templates/>
    </xsl:param>
    <xsl:text>\begin{flushright}
    \textit{ </xsl:text>
    <xsl:copy-of select="$content"/>
    <xsl:text>}
    \end{flushright}</xsl:text>
  </xsl:template>

  <!-- An  attribution's citetitle should be rendered after a line
       break -->
  <xsl:template match="citetitle">
    <xsl:param name="content">
      <xsl:apply-templates/>
    </xsl:param>
    <xsl:text>\\ \quad \hfill </xsl:text>
    <xsl:copy-of select="$content"/>
  </xsl:template>

  <!-- Bibliography and acknowledgements are formatted as numberless
       chapters — And the easiest way to achieve it was abusing the
       "Appendix" definition. -->
  <xsl:template match="appendix">
     <xsl:param name="content">
      <xsl:apply-templates/>
     </xsl:param>
     <xsl:param name="title">
      <xsl:apply-templates/>
     </xsl:param>
     <xsl:text>\chapter*{</xsl:text>
     <xsl:copy-of select="title"/>
     <xsl:text>}</xsl:text>
     <xsl:text>\addcontentsline{toc}{chapter}{</xsl:text>
     <xsl:copy-of select="title"/>
     <xsl:text>}</xsl:text>
     <xsl:copy-of select="$content"/>
  </xsl:template>

  <!-- Place title verso page behind the title page the hard way: put
       colophon content in a file loaded by \maketitle -->
  <xsl:template match="colophon">
    <xsl:variable name="titlepage.verso">
      <xsl:text>\begin{colophon}&#10;</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>\end{colophon}&#10;</xsl:text>
    </xsl:variable>
    <xsl:call-template name="write.text.chunk">
      <!-- The filename must end with 'input.rtex' to be parsed by dblatex -->
      <xsl:with-param name="filename">
	<xsl:text>titlepg.input.rtex</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="method" select="'text'"/>
      <xsl:with-param name="content">
	<xsl:value-of select="$titlepage.verso"/>
      </xsl:with-param>
      <xsl:with-param name="encoding" select="$chunker.output.encoding"/>
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
