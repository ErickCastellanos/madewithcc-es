<!-- Este es un fragmento de DocBook, no un archivo XML legal por sí
     sólo. Se incluye en la construcción del DocBook definitivo desde
     fixup-docbook-translated.xml -->
<preface id="notas-esp">
  <title>Notas a la traducción al español</title>
  <para>La versión original de este libro fue publicada en mayo de
  2017. Muchos entusiastas de Creative Commons lo descargamos
  inmediatamente y comenzamos a leerlo. La primera hojeada fue con
  curiosidad. <quote>¡Vaya! la verdad... ¡está muy bien
  estructurado!</quote> Seguí leyendo un poco más en serio.</para>
  <para>En mi cabeza comenzaron a girar algunos engranes...</para>

  <section>
    <title>Historia técnica y social de la traducción</title>

    <para>Conforme avanzaba en la lectura, no dejaba de darme vueltas
    en la cabeza el haber leído en el <emphasis>blog</emphasis> de
    Petter Reinholdtsen,<footnote><para><ulink
    url="http://people.skolelinux.org/pere/blog/tags/freeculture/"
    /></para></footnote> a quien conozco por ser ambos desarrolladores
    del proyecto Debian, acerca del proceso de traducción al noruego
    que hizo del libro <emphasis>Free Culture</emphasis>, de Lawrence
    Lessig.<footnote><para><ulink url="http://www.free-culture.cc/"
    /></para></footnote> A partir de mi trabajo en la UNAM, he
    participado en un par de proyectos editoriales. El gérmen de la
    traducción se me instaló y comenzó a crecer.</para>

    <para>Petter comenzó a trabajar en su traducción en julio de 2012,
    y terminó la traducción dos años más tarde. Sumando a esto un año
    de trabajo editorial, no resulta muy alejado de mi experiencia en
    la preparación de libros en la Universidad.</para>

    <para>A los pocos días de iniciada mi lectura, publiqué en mi blog
    que tenía la intención de coordinar esta traducción, pidiendo
    recomendaciones para la implementación
    técnica.<footnote><para><ulink
    url="https://gwolf.org/node/4102"></para></footnote> Y en pocos
    días, siguiendo los consejos de Stanislaw Krukowski, Marcos Mezo,
    Cristian Martínez, Antonio Terceiro, estaba todo armado para
    ponerme en acción.</para>

    <para>Varios de los antes mencionados me sugirieron considerar
    <emphasis>Weblate</emphasis>, proyecto desarrollado por Michal
    Čihař, otro compañero del proyecto Debian, un sistema que permite
    la gestión mediante una interfaz Web de traducciones multilíngües
    para los programas que utilizan el marco de traducciones
    <emphasis>Gettext</emphasis>. Desde 2013, además de desarrollar el
    programa, Michal ofrece un servicio de hospedaje de traducciones
    para proyectos de software libre llamado <emphasis>Hosted
    Weblate</emphasis>.<footnote><para><ulink
    url="https://hosted.weblate.org/" /></para></footnote>
    </para>

    <para>A partir de ese punto, el proyecto de esta traducción dejó
    de ser una idea, y comenzó a avanzar, lentamente, pero a
    avanzar. Leo Arias, a quien conozco a partir de su trabajo en la
    comunidad de software libre centroamericana, mostró su interés
    en participar, y una vez que tuvimos las bases técnicas,
    comenzamos a avanzar utilizando <emphasis>Hosted
    Weblate</emphasis>. Al poco tiempo, personas de diferentes países
    fueron encontrando el proyecto de traducción e iniciando
    versiones en sus propios idiomas. Y más personas se fueron
    sumando, sin siquiera anunciarse expresamente, al proyecto.</para>

    <para>Antes de que pudiéramos darnos cuenta, en poco más de tres
    meses de trabajo intensivo, entre cinco personas habíamos
    completado la traducción al español. Este equipo se conformó por:
    Luis Enrique Amaya González (México), Leo Arias
    <quote>elopio</quote> (Costa Rica), Andrés Delgado (Ecuador),
    Evelin Heidel <quote>scann</quote> (Argentina) y Gunnar Wolf
    (México). Y, de forma que ilustra los modelos de creación
    colaborativa que este libro adopta, lo hicimos con apenas
    coordinación entre nosotros, de forma casi absolutamente
    descentralizada.</para>

    <para>En esta sección se presentan las principales herramientas
    para llevar a buen puerto este proyecto. Pero, como podrá
    observarse, no se pueden separar la historia técnica de la historia
    social: esta traducción es un proyecto social de colaboración
    alrededor de un conjunto de creencias y valores que compartimos
    los involucrados. Y eso es lo que nos permite afirmar que también
    en este aspecto somos <emphasis>Hechos con Creative
    Commons</emphasis>.</para>

  </section>
  <section>
    <title>De la traducción y de la edición</title>
    <para>El relato anterior nos lleva apenas hasta finales de
    2017. Estas líneas son de septiembre de 2019. ¿Por qué tanta
    demora?</para>

    <para>La primera vez que hacemos algo, es frecuente que nos falte
    experiencia para prever sus implicaciones. Como equipo de
    traducción, fuimos eficientes... Pero el resultado no era de la
    calidad necesaria, ni para representar correctamente al nombre
    <emphasis>Creative Commons</emphasis>, ni para una publicación que
    lleve el sello de la Universidad Nacional Autónoma de México. Este
    libro tuvo un muy largo proceso editorial, homogeneizando el
    lenguaje y los términos utilizados. Tengo que agradecer
    expresamente al muy paciente y muy profesional trabajo de Marisol
    Simón, la revisora de estilo a quien estoy seguro que nuestro
    trabajo le arrancó más que un par de lágrimas y canas; el
    resultado de su trabajo se nota claramente.</para>

    <para>En el trabajo editorial acordamos traducciones adecuadas
    para muchos de los términos empleados, intentando presentar una
    lectura clara y natural. Esperamos habernos alejado de los
    modismos y localismos que usamos sin darnos cuenta en nuestra
    comunicación oral. Esperamos también que los términos que
    empleamos resulten claros y naturales para los lectores de todas
    las latitudes.</para>

  </section>
  <section>
    <title>La traducción de <emphasis>The Commons</emphasis></title>

    <para>Tenemos que hacer mención especial de un término fundamental
    que nos resultó complicado de traducir, y central a lo largo del
    libro: <emphasis>The Commons</emphasis>. Este término es de uso
    muy frecuente en el inglés, generando una serie de términos
    difíciles de traducir al español, como
    <emphasis>commonwealth</emphasis> (que normalmente se traduce como
    <quote>mancomunidad</quote>, sin que esto ayude a comprender el
    significado del término), <emphasis>common law</emphasis>
    (traducido frecuentemente como <quote>derecho anglosajón</quote>,
    pero inexacto al convertir un término descriptivo en la referencia
    al sistema legal que lo diseminó). La multiplicidad de sentidos
    contenidos en el término <emphasis>commons</emphasis> puede llevar
    a confusiones sobre la idea a la que se refiere: si es a un bien
    común concreto, o a un recurso determinado, incluso a la idea de
    estar en común o la comunidad que se relaciona con dicho discurso
    y los modos de gestión del mismo. </para>

    <para>Dado que este es un libro cuyo eje fundamental es
    <emphasis>Creative Commons</emphasis>, ¿cómo puede entenderse este
    término en español?</para>

    <para>Hemos encontrado el uso de términos como el
    <emphasis>procomún</emphasis> (neologismo generado a partir de la
    contracción de <quote>provecho</quote> y <quote>común</quote>),
    los <emphasis>bienes comunes</emphasis>, los <emphasis>bienes
    comunales</emphasis>, los <emphasis>recursos de uso
    común</emphasis>, el <emphasis>patrimonio común</emphasis> o los
    <emphasis>comunes</emphasis>. A lo largo del libro, estandarizamos
    el uso del término por este último por presentarse como el más
    genérico y el mejor para describir los distintos tipos de uso,
    aunque manteniendo un poco de flexibilidad, particularmente en los
    primeros capítulos, en los que se aborda el proceso histórico de
    desarrollo y administración de los recursos.</para>

    <para><emphasis>Creative Commons</emphasis> podría entonces
    traducirse por <quote>comunes creativos</quote> o <quote>comunes
    de la creatividad</quote>. Claro, mantendremos el uso de
    <emphasis>Creative Commons</emphasis> porque no nos referimos con
    ello a un concepto genérico, sino al conjunto de licencias que
    presentaremos en el capítulo 3, y a la organización social y
    política estructurada alrededor de la promoción de dichas
    licencias.</para>
  </section>
  <blockquote><attribution>Gunnar Wolf <citetitle>Instituto de
  Investigaciones Económicas,
  UNAM</citetitle></attribution></blockquote>
</preface>
