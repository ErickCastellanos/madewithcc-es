#
# As of October 2017, I'm doing some Spanish-specific adjustments to
# this Makefile; they should be generalized... But I'm taking a step at a time :)
# -GW
#
SOURCE=MadewithCreativeCommonsmostup-to-dateversion

# Versioning: I'm using the date on which I got the source document
# (2017-06-09), plus an incrementing local part, that *should*
# increase every time the generated Markdown is modified.
VERSION=20170609-2

# pandoc version 1.17.2~dfsg-3 do not understand
# --top-level-diversion, while version 1.19.2.4~dfsg-1+b3 do.
PANDOC_OPTS = \
  --standalone \
  --top-level-division=part \
  -t docbook4 \
  -f markdown+inline_notes+ascii_identifiers

LANGS := $(shell ls po/*/mwcc.po|cut -d/ -f2)

DESTDIR =

docdir = /usr/share/doc/madewithcc

install = install

all: pdf epub

install:
	$(install) *.pdf *.epub $(DESTDIR)/$(docdir)

distclean: clean
	rm -f $(SOURCE).md
clean:
	for LANG in $(LANGS); do \
	    rm -f $(SOURCE).$$LANG.md ;\
	done
	for EXT in aux cb cb2 glo idx lof log tex toc xml pdf epub; do \
	    rm -f $(SOURCE).$$EXT ;\
	    for LANG in $(LANGS); do \
	        rm -f $(SOURCE).$$LANG.$$EXT ;\
	    done; \
        done
	rm -f xsl-fo-$(SOURCE).*.pdf
	rm -f xsl-fo-$(SOURCE).*.fo
	$(RM) *~

$(SOURCE).md: $(SOURCE).odt fixup.rb
	TEMP=`tempfile -p mwcc` && \
	LC_ALL=C.UTF-8 pandoc -f odt $(SOURCE).odt -t markdown > $$TEMP && \
	LC_ALL=C.UTF-8 ruby fixup.rb $$TEMP $(SOURCE).md && \
	rm $$TEMP

pot: po/mwcc.pot po/*/mwcc.po
PO4AOPTS =  \
	-o nodefault="<imageobject> <copyright> <publisher> <address> <city>" \
	-o untranslated="<imageobject>" \
	-o translated='<copyright> <publisher><publisername> <publisher><address><city>' 
po/mwcc.pot: $(SOURCE).xml
	TEMP=`tempfile -p mwcc` && \
	po4a-gettextize -f docbook -m $(SOURCE).xml -p $$TEMP -M utf-8 \
		$(PO4AOPTS) \
		--package-name 'Made with Creative Commons' \
		--package-version $(VERSION) && \
	echo '# MADE WITH CREATIVE COMMONS' > $@ && \
	echo '# Copyright (C) 2017 by Creative Commons.' >> $@ && \
	echo '# This file is published under a Creative Commons Attribution-ShareAlike license (CC BY-SA), version 4.0' >> $@ && \
	echo '# Authors: Paul Stacey and Sarah Hinchliff Pearson' >> $@ && \
	tail --lines=+5 $$TEMP >> $@ && \
	rm $$TEMP

po/*/mwcc.po: po/mwcc.pot
	msgmerge --previous $@ po/mwcc.pot -U
	touch $@

$(SOURCE).de.xml: $(SOURCE).xml po/de/mwcc.po
	po4a-translate -f docbook -m $(SOURCE).xml -p po/de/mwcc.po -l $@ -l $@ -L utf-8 -M utf-8 $(PO4AOPTS) -k 0

$(SOURCE).el.xml: $(SOURCE).xml po/el/mwcc.po
	po4a-translate -f docbook -m $(SOURCE).xml -p po/el/mwcc.po -l $@ -l $@ -L utf-8 -M utf-8 $(PO4AOPTS) -k 0

$(SOURCE).es.xml: $(SOURCE).xml po/es/mwcc.po fixup-docbook-translated.rb
	TEMP=`tempfile -p mwcc` && \
	  po4a-translate -f docbook -m $(SOURCE).xml -p po/es/mwcc.po -l $$TEMP -l $$TEMP -L utf-8 -M utf-8 $(PO4AOPTS) -k 0 && \
	  ruby fixup-docbook-translated.rb es $$TEMP $@ && \
	  rm -f $$TEMP

$(SOURCE).nb.xml: $(SOURCE).xml po/nb/mwcc.po
	po4a-translate -f docbook -m $(SOURCE).xml -p po/nb/mwcc.po -l $@ -l $@ -L utf-8 -M utf-8 $(PO4AOPTS) -k 0

$(SOURCE).nl.xml: $(SOURCE).xml po/nl/mwcc.po
	po4a-translate -f docbook -m $(SOURCE).xml -p po/nl/mwcc.po -l $@ -l $@ -L utf-8 -M utf-8 $(PO4AOPTS) -k 0

$(SOURCE).pl.xml: $(SOURCE).xml po/pl/mwcc.po
	po4a-translate -f docbook -m $(SOURCE).xml -p po/pl/mwcc.po -l $@ -l $@ -L utf-8 -M utf-8 $(PO4AOPTS) -k 0

$(SOURCE).uk.xml: $(SOURCE).xml po/uk/mwcc.po
	po4a-translate -f docbook -m $(SOURCE).xml -p po/uk/mwcc.po -l $@ -l $@ -L utf-8 -M utf-8 $(PO4AOPTS) -k 0

$(SOURCE).xml: $(SOURCE).md fixup-docbook.rb
	TEMP=`tempfile -p mwcc` && \
	  pandoc -s -o $$TEMP $(PANDOC_OPTS) $(SOURCE).md && \
	  ruby fixup-docbook.rb $$TEMP $@ && \
	  rm $$TEMP
	xmllint --format $@ > $@.new && mv $@.new $@

pdf: $(SOURCE).pdf
	for LANG in $(LANGS); do \
	    $(MAKE) $(SOURCE).$$LANG.pdf ; \
	done

DBLATEX_OPTS = -T simple -t pdf -b xetex
%.es.pdf: %.es.xml extra/pdf.xsl myclass.cls
	rm -f extra/pdf_es.xsl myclass_es.cls
	sed s/myclass/myclass_es/ extra/pdf.xsl > extra/pdf_es.xsl
	ruby -e 'begin; while (data = readline) ; puts data;if (data == "\\makeatletter\n") ;puts "\\input{extra/es/unam_definitions}\n"; end;end; rescue EOFError;true;end' < myclass.cls > myclass_es.cls
	dblatex $(DBLATEX_OPTS) -p extra/pdf_es.xsl $^
	rm -f extra/pdf_es.xsl myclass_es.cls

%.pdf: %.xml extra/pdf.xsl myclass.cls
	dblatex $(DBLATEX_OPTS) -p extra/pdf.xsl $^

epub: $(SOURCE).epub
	for LANG in $(LANGS); do \
	    $(MAKE) $(SOURCE).$$LANG.epub ; \
	    flightcrew-cli $(SOURCE).$$LANG.epub || exit 1 ; \
	done
%.epub: %.xml
	dbtoepub $^

# Useful for spell checking the text
%.txt: %.xml
	xmlto txt $<

XMLLINTOPTS = --nonet --noout  --xinclude --postvalid
lint: $(SOURCE).xml
	xmllint $(XMLLINTOPTS) $^
lint-es: $(SOURCE).es.xml
	xmllint $(XMLLINTOPTS) $^
lint-nb: $(SOURCE).nb.xml
	xmllint $(XMLLINTOPTS) $^

check-urls:
	retval=0; for u in $$(perl -n -e'/<ulink url="(.+)"\/>/ && print "$$1\n"' MadewithCreativeCommonsmostup-to-dateversion.xml | sort -u); do \
	  GET -H "User-Agent: Docbook XML URL checker" "$$u" > /dev/null || echo "error fetching $$u"; retval=1; \
	done; exit $$retval

# Experimental build rule to test Docbook XSL + FOP processor 
xsl-fo-%.pdf: %.xml extra/stylesheet-fo.xsl
	xsltproc --output $(subst .pdf,.fo,$@) \
	  extra/stylesheet-fo.xsl $^; \
	fop -fo $(subst .pdf,.fo,$@) -pdf $@

status:
	for LANG in $(LANGS); do \
	   printf "$$LANG "; msgfmt -o /dev/null --statistics po/$$LANG/mwcc.po; \
	done

.SUFFIXES: .xml .md
